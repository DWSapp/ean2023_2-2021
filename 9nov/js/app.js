/* 
Wait for DOM content
*/
document.addEventListener('DOMContentLoaded', () => {
    /* 
    Declarations
    */
        // Create varaible for debug
        const debug = false;

        // Create an object with all main sections
        let mainSections = {
            'section-user-list': document.querySelector('#section-user-list'),
            'section-user-info': document.querySelector('#section-user-info'),
            'section-album-info': document.querySelector('#section-album-info'),
            'section-photo': document.querySelector('#section-photo')
        }

        // Define API start URL
        const apiUrl = 'https://jsonplaceholder.typicode.com';
    //
    
    /* 
    Functions
    */
        /**
         * Function getJsonCollection
         * This function is used to fetch data from an API
         * @param {string} endpoint - The API endpoint (ex. "photos?albumId=1").
         * @param {markup} section - The HTML markup to add children nodes.
         */
        const getJsonCollection = (endpoint, section) => {
            // Define new fetch request
            const fetchRequest = new FetchRequest(
                `${apiUrl}/${endpoint}`,
                'json'
            )

            // Set request HEADER
            fetchRequest.init()
            
            // Send request and use Promise
            fetchRequest.sendRequest()
            .then( fetchResponse => {
                // Get section index
                let sectionArray = [];
                for( let prop in mainSections ){
                    sectionArray.push( mainSections[prop] )
                }

                // Get section to hide
                let selectedSection = undefined;
                for( let i = 0; i < sectionArray.length; i++ ){
                    // Define selected section
                    if(section.id == sectionArray[i].id){
                        // Define selected section
                        selectedSection = i;
                    }
                }
                
                // Check if fetchResponse is an array
                if( fetchResponse.length !== undefined ){
                    // Loop on fetchResponse
                    for( let item of fetchResponse ){
                        /**
                         * Function createElements
                         * This function is used create to HTML markups
                         * @param {string} endpoint - The API endpoint (ex. "photos?albumId=1").
                         * @param {object} item - The object to use to add markups
                         */
                        let domContent = createElements(endpoint, item)
                        
                        // Create new LI
                        let listItem = document.createElement('li');

                        // Create new BUTTON
                        let button = document.createElement('button')
                        button.innerText = 'See more'
                        button.setAttribute('data-id', item.id)

                        // Bind click on button
                        button.addEventListener('click', event => {
                            // Check endpoint
                            let objectType = undefined;
                            if( endpoint.indexOf('users') !== -1 ){
                                objectType = 'users'
                            }
                            else if( endpoint.indexOf('albums') !== -1 ){
                                objectType = 'albums'
                            }
                            else if( endpoint.indexOf('photos') !== -1 ){
                                objectType = 'photos'
                            }

                            // Bind click
                            onClick(objectType, event.target.getAttribute('data-id'))
                        })

                        // Add button in DOM content
                        domContent.push(button);

                        // Add DOM content in the list item
                        for( let markup of domContent ){
                            listItem.appendChild(markup)
                        }

                        // Add list item in the section and display the section
                        section.children[1].appendChild(listItem);

                        // Debug
                        if(debug){
                            console.log('From ARRAY', endpoint, listItem)
                        }
                    }
                }
                else{
                    // Get HTML markups
                    let domContent = createElements(endpoint, fetchResponse)

                    // Add list item in the section and display the section
                    section.appendChild( domContent[0] );

                    // Debug
                    if(debug){
                        console.log('From OBJECT', endpoint, section)
                    }
                }
                
                // Check section to hide
                if( selectedSection > 0 ){
                    // Hide prev section
                    sectionArray[ selectedSection - 1 ].classList.remove('show');
                    setTimeout(() => {
                        sectionArray[ selectedSection - 1 ].classList.remove('open');
                    }, 500);

                    // Debug
                    if(debug){
                        console.log('Hide prev section', sectionArray[ selectedSection - 1 ])
                    }
                }
                
                // Wait for prev section deletation
                setTimeout(() => {
                    // Change display to BLOCK
                    section.classList.add('open');
                    
                    // Wait a little
                    setTimeout(() => {
                        // Change OPACITY to 1
                        section.classList.add('show');

                    }, 100);
                }, selectedSection > 0 ? 510 : 0);
            })
            .catch( fetchError => {
                // Debug
                if(debug){
                    console.log('Fetch error', fetchError)
                }
            })
        }

        /**
         * Function createElements
         * This function is used create to HTML markups
         * @param {string} endpoint - The API endpoint (ex. "photos?albumId=1").
         * @param {object} item - The object to use to add markups
         */
        const createElements = (endpoint, item) => {
            // Check enpoint
            if(  endpoint.indexOf('users') !== -1 ){
                // Create elements for name, city, email and website
                let name = document.createElement('p');
                name.innerHTML = item.name;

                let city = document.createElement('p');
                city.innerText = item.address.city;

                let email = document.createElement('a');
                email.innerText = item.email;
                email.href = `mailto:${item.email}`;

                let website = document.createElement('a');
                website.innerText = item.website;
                website.href = 'http://' + item.website;
                website.target = '_blank';

                // Debug
                if(debug){
                    console.log('createElements: users', [ name, city, email, website ])
                }

                // Return elements
                return [ name, city, email, website ];
            }
            else if( endpoint.indexOf('albums') !== -1 ){
                // endpoint === `albums?userId=${item.id}`
                let title = document.createElement('p');
                title.innerHTML = item.title;

                // Debug
                if(debug){
                    console.log('createElements: albums', [ title ])
                }

                // Return elements
                return [ title ];
            }
            else if( endpoint.indexOf('photos') !== -1 ){
                // endpoint === `albums?userId=${item.id}`
                let figure = document.createElement('figure');
                let figcaption = document.createElement('figcaption');
                
                // Set image
                let img = document.createElement('img');
                img.src = endpoint.indexOf('photos/') !== -1 ? item.url : item.thumbnailUrl;
                img.alt = item.title;

                // Set figcaption
                figcaption.innerText = item.title;
                
                // Set figure
                figure.appendChild(img)
                figure.appendChild(figcaption)

                // Debug
                if(debug){
                    console.log('createElements: photos', [ figure ])
                }

                // Return elements
                return [ figure ];
            }
        }

        /**
         * Function onClick
         * This function is used bind clicks on BUTTON markup
         * @param {string} type - The type of object cicked on (eq. 'users '|| 'albums' || 'photos')
         * @param {string} id - The id of the object to fecth
         */
        const onClick = (type, id) => {
            // Check type to fetch the correct values
            if( type === 'users' ){
                /**
                 * Call getJsonCollection function
                 * @param {string} endpoint = `albums?userId=${id}`
                 * @param {markup} section = mainSections['section-user-info']
                 */
                getJsonCollection(`albums?userId=${id}`, mainSections['section-user-info'])
            }
            else if( type === 'albums' ){
                /**
                 * Call getJsonCollection function
                 * @param {string} endpoint = `photos?albumId=${id}`
                 * @param {markup} section = mainSections['section-album-info']
                 */
                getJsonCollection(`photos?albumId=${id}`, mainSections['section-album-info'])
            }
            else if( type === 'photos' ){
                /**
                 * Call getJsonCollection function
                 * @param {string} endpoint = `photos/${id}`
                 * @param {markup} section = mainSections['section-photo']
                 */
                getJsonCollection(`photos/${id}`, mainSections['section-photo']);
            }

            // Debug
            if(debug){
                console.log('onClick', type, id)
            }
        }
    //
    
    /* 
    Start
    */
        /**
         * Call getJsonCollection function
         * @param {string} endpoint = 'users'
         * @param {markup} section = mainSections['section-user-list']
         */
        getJsonCollection('users', mainSections['section-user-list'])
    //
})