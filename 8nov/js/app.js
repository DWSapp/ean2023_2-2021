/* 
Wait for DOM content
*/
document.addEventListener('DOMContentLoaded', () => {
    /* 
    Declarations
    */
        // Create an object with all main sections
        let mainSections = {
            'section-user-list': document.querySelector('#section-user-list'),
            'section-user-info': document.querySelector('#section-user-info'),
            'section-album-info': document.querySelector('#section-album-info'),
            'section-photo': document.querySelector('#section-photo')
        }

        const apiUrl = 'https://jsonplaceholder.typicode.com';
    //
    
    /* 
    Functions
    */
        // Create function to get all users
        const getJsonCollection = (endpoint, callback, method = 'GET') => {
            // Define new fetch request
            const fetchRequest = new FetchRequest(
                `${apiUrl}/${endpoint}`,
                'json'
            )

            // Set request HEADER
            fetchRequest.init()
            
            // Send request and use Promise
            fetchRequest.sendRequest()
            .then( fetchResponse => {
                // Call function callback
                callback(fetchResponse)
            })
            .catch( fetchError => {
                console.log(fetchError)
            })
        }

        // Create function to bind click
        const onClick = (type, id) => {
            // Check type to fetch the correct values
            if( type === 'users' ){
                // Get users info by id and album list by userId
                // albums?userId=1 || users/1/albums
                getJsonCollection(`albums?userId=${id}`, displayUserInfo)
            }
            else if( type === 'albums' ){
                // Get album info by id and photo list by albumId
                // photos?albumId=1 || users/1/photos
                getJsonCollection(`photos?albumId=${id}`, displayAlbumInfo)
            }
            else if( type === 'photos' ){
                // Get photo info by id
                getJsonCollection(`photos/${id}`, displayPhoto);
            }
        }

        // Create a function to create user list
        const displayUserList = (data) => {
           for( let item of data ){
               // Create all MARKUPS
               let userLi = document.createElement('li')

               let userName = document.createElement('p')
               userName.innerText = item.name;

               let userCity = document.createElement('p')
               userCity.innerText = item.address.city;

               let userEmail = document.createElement('a')
               userEmail.innerText = item.email;
               userEmail.href = `mailto:${item.email}`

               let userWebsite = document.createElement('a')
               userWebsite.innerText = item.website;
               userWebsite.href = 'http://' + item.website
               userWebsite.target = '_blank'

               let userLink = document.createElement('button')
               userLink.innerText = 'See more'
               userLink.setAttribute('data-id', item.id)

               // Bind click on userLink
               userLink.addEventListener('click', event => {
                   // Bind click
                   onClick('users', event.target.getAttribute('data-id'))
               })

               // Create an array with all MARKUPS
               const htmlTags = [ userName, userCity, userEmail, userWebsite, userLink ]
               
               // Add all markups
               for( let tag of htmlTags ){
                   userLi.appendChild(tag)
               }

               // Add li in #user-list-container
               //=> https://developer.mozilla.org/en-US/docs/Web/API/Node/children
               mainSections['section-user-list'].children[1].appendChild(userLi);
            }

            // Display user list
            mainSections['section-user-list'].classList.add('open')
        }

        // Create a function to create user info
        const displayUserInfo = (data) => {
            // Hide user list
            mainSections['section-user-list'].classList.remove('open')

            // Display album list (title, button)
            for( let item of data ){
                // Create all MARKUPS
               let albumLi = document.createElement('li')

               let albumTitle = document.createElement('p')
               albumTitle.innerText = item.title;

               let albumLink = document.createElement('button')
               albumLink.innerText = 'See photos';
               albumLink.setAttribute('data-id', item.id)

               // Bind click on albumLink
               albumLink.addEventListener('click', event => {
                    // Bind click
                    onClick('albums', event.target.getAttribute('data-id'))
               })
               
               // Add all markups
               for( let tag of [ albumTitle, albumLink ] ){
                    albumLi.appendChild(tag)
               }

               // Add li in #user-list-container
               //=> https://developer.mozilla.org/en-US/docs/Web/API/Node/children
               mainSections['section-user-info'].children[1].appendChild(albumLi)
            }

            // Display album list
            mainSections['section-user-info'].classList.add('open')
        }

        // Create a function to create album info
        const displayAlbumInfo = (data) => {
            // Hide album info
            mainSections['section-user-info'].classList.remove('open')

            // Display photo list (thumbnailUrl, title, button)
            for( let item of data ){
                // Create all MARKUPS
                let photoLi = document.createElement('li')
                let photoFigure = document.createElement('figure')
                let photoFigcaption = document.createElement('figcaption')
                let photoImage = document.createElement('img')

                // Set image
                photoImage.src = item.thumbnailUrl
                photoImage.alt = item.title
                photoFigcaption.innerText = item.title

                // Set link
                let photoLink = document.createElement('button')
                photoLink.innerText = 'See photos'
                photoLink.setAttribute('data-id', item.id)

                // Bind click on photoLink
                photoLink.addEventListener('click', event => {
                    // Bind click
                    onClick('photos', event.target.getAttribute('data-id'))
                })

                // Add BUTTON in FIGCAPTION
                photoFigcaption.appendChild(photoLink)
                
                // Add IMG and FIGCAPTION in FIGURE
                for( let tags of [ photoImage, photoFigcaption ] ){
                    photoFigure.appendChild(tags)
                }

                // Add FIGURE in LI
                photoLi.appendChild(photoFigure)

                // Add li in #section-album-info
                //=> https://developer.mozilla.org/en-US/docs/Web/API/Node/children
                mainSections['section-album-info'].children[1].appendChild(photoLi)
            }

            // Display album info
            mainSections['section-album-info'].classList.add('open')
        }

        // Create a function to create photo
        const displayPhoto = (item) => {
            // Hide album info
            mainSections['section-album-info'].classList.remove('open')

            // Set imapge
            let itemFigure = document.createElement('figure')
            let itemFigcaption = document.createElement('figcaption')
            let itemImage = document.createElement('img')

            // Set image
            itemImage.src = item.url
            itemImage.alt = item.title
            itemFigcaption.innerText = item.title

            // Add IMG and FIGCAPTION in FIGURE
            for( let tags of [ itemImage, itemFigcaption ] ){
                itemFigure.appendChild(tags)
            }

            // Add li in #section-album-info
            //=> https://developer.mozilla.org/en-US/docs/Web/API/Node/children
            mainSections['section-photo'].appendChild(itemFigure)

            // Display album info
            mainSections['section-photo'].classList.add('open')
        }
    //
    
    /* 
    Start
    */
        // Get all users
        getJsonCollection('users', displayUserList)
    //
})

window.addEventListener('resize', (event) => {
    // Remove all classes
    document.querySelector('body').classList.remove('mobile-theme', 'medium-theme', 'large-theme')
    
    // Add classe
    if( window.innerWidth < 500 ){
        document.querySelector('body').classList.add('mobile-theme')
    }
    else if( window.innerWidth > 501 && window.innerWidth < 600 ){
        document.querySelector('body').classList.add('medium-theme')
    }
    else if( window.innerWidth > 601 ){
        document.querySelector('body').classList.add('large-theme')
    }
})