/* 
Get HTML markup in Javascript
=> https://developer.mozilla.org/fr/docs/Web/API/Document/querySelector
*/
    let burgerIcon = document.querySelector('#burger-icon');
    let navigation = document.querySelector('nav ul');
//

/* 
Bind click on burgerIcon
=> https://developer.mozilla.org/fr/docs/Web/API/EventTarget/addEventListener
*/
    burgerIcon.addEventListener('click', function(event){
        navigation.classList.toggle('open');
    })
//


/* 
jQuery version
=> https://jquery.com

    let burgerIcon = $('#burger-icon');
    let navigation = $('nav ul');

    burgerIcon.click( function(event){
        navigation.toggleClass('open')
    })
*/