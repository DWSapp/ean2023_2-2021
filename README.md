# Livecampus index des liens et supports

![](https://i.imgur.com/gqrO2z8.png)

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) - All rights reserved for educational purposes only*

https://gitlab.com/DWSapp/ean2023_2-2021

---

<br>

# Supports de cours

- **WebStack : Empiler les technologies** https://hackmd.io/@teach-supports/rJr2NBwE_
- **Application native, hybride, and repeat...** https://hackmd.io/@teach-supports/r1pYVSv4_
- **Versioning et gestion de code** https://hackmd.io/@teach-supports/SkrRrHw4O
- **Afficher des informations sur Internet** https://hackmd.io/@teach-supports/ByZnBSv4O
- **Les feuilles de style en cascade** https://hackmd.io/@teach-supports/ByK5SBvV_
- **Javascript : Les bases de la programmation** https://hackmd.io/@teach-supports/HycpVHDNd

<br><br>

# Javascript

- **Ajax (informatique)** https://fr.wikipedia.org/wiki/Ajax_(informatique)
- **XMLHttpRequest** https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest
- **API Fetch** https://developer.mozilla.org/fr/docs/Web/API/Fetch_API
- **EventListener** https://developer.mozilla.org/fr/docs/Web/API/EventListener

# Ressources en ligne
- **Equivalences entre Linux et Windows** https://hackmd.io/@teach-supports/Hk16GCbNK
- **Schema ORG** https://schema.org
- **Live Server** https://www.npmjs.com/package/live-server
- **NodeJS** https://nodejs.org/en/
- **Design System** https://www.ux-republic.com/retour-dexperience-le-design-system-en-10-etapes/
- **Secure Shell** https://fr.wikipedia.org/wiki/Secure_Shell
- **Modèle-vue-contrôleur** https://fr.wikipedia.org/wiki/Modèle-vue-contrôleur
- **Milligram, Micro framework CSS** https://milligram.io/

<br><br>

# Documentaires

- **Vuejs the documentary** https://www.youtube.com/watch?v=OrxmtDw4pVI&t=2s
- **Les invisibles** https://www.france.tv/slash/invisibles/
- **Derrière nos écrans de fumée** https://www.netflix.com/fr/title/81254224
- **Propaganda, la fabrique du consentement** https://www.imagotv.fr/documentaires/propaganda-la-fabrique-du-consentement/film/1
