/* 
Create JS classe
*/

class FetchRequest {
    // Classe constructor: used to inject value in the classe
    constructor( url ){
        // Classe property
        this.url = url;
    }

    // Classe method
    init = function(){
        console.log('Init FetchRequest', this.url)
    }
}